﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;
        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удалить сотрудника по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);

            return NoContent();
        }

        /// <summary>
        /// добавить нового сотрудника
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync(Employee employee)
        {
            foreach (var role in employee.Roles)
            {
                var result =  await _rolesRepository.GetByIdAsync(role.Id);
                if (result == null)
                    return NotFound();
            }

            var employeeid = await _employeeRepository.AddAsync(employee);

            return employeeid;
        }

        /// <summary>
        /// обновить данные о сотруднике
        /// </summary>
        /// <param name="updatedEmployee"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(Employee updatedEmployee)
        {
            if (updatedEmployee.Roles != null)
            {
                foreach (var role in updatedEmployee.Roles)
                {
                    var result = await _rolesRepository.GetByIdAsync(role.Id);
                    if (result == null)
                        return NotFound();
                }
            }

            var employee = await _employeeRepository.GetByIdAsync(updatedEmployee.Id);
            if (employee == null)
                return NotFound();

            employee.FirstName = updatedEmployee.FirstName ?? employee.FirstName;
            employee.LastName = updatedEmployee.LastName ?? employee.LastName;
            employee.Roles = updatedEmployee.Roles ?? employee.Roles;
            employee.Email = updatedEmployee.Email ?? employee.Email;
            employee.AppliedPromocodesCount = updatedEmployee.AppliedPromocodesCount;
            await _employeeRepository.UpdateAsync(employee);

            return NoContent();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IDictionary<Guid, T> Data { get; set; }

        public InMemoryRepository(IDictionary<Guid, T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data.Values.ToList<T>());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Value.Id == id).Value);
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.Run(() => {
                Data.Remove(id);
            });
        }

        public Task<Guid> AddAsync(T item)
        {
            item.Id = Guid.NewGuid();
            return Task<Guid>.Run(() => {
                Data.Add(item.Id, item);
                return item.Id;
            });
        }

        public Task UpdateAsync(T item)
        {
            return Task<Guid>.Run(() => {
                 Data[item.Id] = item;
            });
        }
    }
}